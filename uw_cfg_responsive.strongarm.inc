<?php

/**
 * @file
 * uw_cfg_responsive.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_cfg_responsive_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'responsive_menu_combined_display_parent_title';
  $strongarm->value = 1;
  $export['responsive_menu_combined_display_parent_title'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'responsive_menu_combined_html_tags';
  $strongarm->value = 'h2';
  $export['responsive_menu_combined_html_tags'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'responsive_menu_combined_settings';
  $strongarm->value = array(
    'menu-uw-menu-global-header' => array(
      'friendly_name' => 'UWaterloo',
      'enabled' => 1,
      'weight' => '14',
      'id' => 'menu-uw-menu-global-header',
      'pid' => '',
      'depth' => '',
    ),
    'menu-audience-menu' => array(
      'friendly_name' => 'Information for',
      'enabled' => 1,
      'weight' => '14',
      'id' => 'menu-audience-menu',
      'pid' => 'main-menu',
      'depth' => '',
    ),
    'main-menu' => array(
      'friendly_name' => 'This site',
      'enabled' => 1,
      'weight' => '13',
      'id' => 'main-menu',
      'pid' => '',
      'depth' => '',
    ),
    'management' => array(
      'friendly_name' => 'Management',
      'enabled' => 0,
      'weight' => '15',
      'id' => 'management',
      'pid' => '',
      'depth' => '',
    ),
    'navigation' => array(
      'friendly_name' => 'Navigation',
      'enabled' => 0,
      'weight' => '16',
      'id' => 'navigation',
      'pid' => '',
      'depth' => '',
    ),
    'menu-site-management' => array(
      'friendly_name' => 'Site management',
      'enabled' => 0,
      'weight' => '17',
      'id' => 'menu-site-management',
      'pid' => '',
      'depth' => '',
    ),
    'user-menu' => array(
      'friendly_name' => 'User menu',
      'enabled' => 0,
      'weight' => '18',
      'id' => 'user-menu',
      'pid' => '',
      'depth' => '',
    ),
    'menu-site-manager-vocabularies' => array(
      'friendly_name' => 'Vocabularies',
      'enabled' => 0,
      'weight' => '19',
      'id' => 'menu-site-manager-vocabularies',
      'pid' => '',
      'depth' => '',
    ),
  );
  $export['responsive_menu_combined_settings'] = $strongarm;

  return $export;
}
